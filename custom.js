jQuery(document).ready(function($) {
    function close_accordion_section() {
        $('.accordion .accordion-section-title').removeClass('active');
        $('.accordion .accordion-section-content').slideUp(300).removeClass('open');
        $('.accordion .IconOpen').css('display', 'block');
        $('.accordion .IconClose').css('display', 'none');
    }

    $('.accordion-section-title').click(function(e) {
        var currentAttrValue = $(this).attr('href');
        if ($(this).hasClass('active')) {
            close_accordion_section();
        } else {
            close_accordion_section();

            $(this).addClass('active');

            $('.accordion ' + currentAttrValue).slideDown(300).addClass('open');

            $(this).find('.IconOpen').css('display', 'none');
            $(this).find('.IconClose').css('display', 'block');
        }

        e.preventDefault();
    });

    //fix bug open close
    $('.accordion-title').click(function(e) {
        var accordionSection = $(this).closest('.accordion-section');
        var isOpen = accordionSection.hasClass('open');

        if (isOpen) {
            accordionSection.removeClass('open');
            $(this).find('.IconOpen').css('display', 'block');
            $(this).find('.IconClose').css('display', 'none');
        } else {
            accordionSection.addClass('open');
            $(this).find('.IconOpen').css('display', 'none');
            $(this).find('.IconClose').css('display', 'block');
        }

        e.preventDefault();
    });
});