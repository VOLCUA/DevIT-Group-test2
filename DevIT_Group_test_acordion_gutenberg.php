<?php
/**
 * Plugin Name: DevIT Group Test Accordion Gutenberg
 * Description: Custom Gutenberg accordion block.
 * Version: 1.0
 * Author: VOLCUA
 */

function devit_group_test_accordion_gutenberg_register_block() {
    wp_register_script(
        'devit-group-test-accordion-gutenberg-editor-script',
        plugins_url( 'build/index.js', __FILE__ ),
        array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor' ),
        filemtime( plugin_dir_path( __FILE__ ) . 'build/index.js' )
    );

    register_block_type( 'devit-group-test-accordion-gutenberg/accordion', array(
        'editor_script' => 'devit-group-test-accordion-gutenberg-editor-script',
    ) );
}

add_action( 'init', 'devit_group_test_accordion_gutenberg_register_block' );

function devit_group_test_scripts() {
    wp_enqueue_style('devit_group_test_accordion_gutenberg', plugin_dir_url(__FILE__) . 'style.css');
    wp_enqueue_script('devit_group_test_accordion_gutenberg', plugin_dir_url(__FILE__) . 'custom.js', array('jquery'), null, true);
}

add_action('wp_enqueue_scripts', 'devit_group_test_scripts');