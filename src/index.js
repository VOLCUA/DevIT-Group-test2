const { registerBlockType } = wp.blocks;
const { TextControl, Button } = wp.components;
const { useState } = wp.element;

registerBlockType('devit-group-test-gutenberg/accordion', {
    title: 'Акордіон',
    icon: 'shield',
    category: 'common',
    attributes: {
        items: {
            type: 'array',
            default: [
                { title: 'Заголовок 1', content: 'Вміст 1', isOpen: false },
                { title: 'Заголовок 2', content: 'Вміст 2', isOpen: false },
            ],
        },
    },

    edit: function (props) {
        const { attributes, setAttributes } = props;

        const addAccordionItem = () => {
            const newItems = [...attributes.items, { title: '', content: '', isOpen: false }];
            setAttributes({ items: newItems });
        };

        const updateAccordionItemTitle = (newValue, index) => {
            const newItems = [...attributes.items];
            newItems[index].title = newValue;
            setAttributes({ items: newItems });
        };

        const updateAccordionItemContent = (newValue, index) => {
            const newItems = [...attributes.items];
            newItems[index].content = newValue;
            setAttributes({ items: newItems });
        };

        const toggleAccordionItem = (index) => {
            const newItems = [...attributes.items];
            newItems[index].isOpen = !newItems[index].isOpen;
            setAttributes({ items: newItems });
        };

        const deleteAccordionItem = (index) => {
            const newItems = [...attributes.items];
            newItems.splice(index, 1);
            setAttributes({ items: newItems });
        };

        return (
            <div className={props.className}>
                <Button onClick={addAccordionItem}>Додати новий елемент</Button>
                {attributes.items.map((item, index) => (
                    <div key={index} className={`accordion-item ${item.isOpen ? 'open' : ''}`}>
                        <TextControl
                            label={`Заголовок елементу ${index + 1}`}
                            value={item.title}
                            onChange={(value) => updateAccordionItemTitle(value, index)}
                        />
                        <TextControl
                            label={`Вміст елементу ${index + 1}`}
                            value={item.content}
                            onChange={(value) => updateAccordionItemContent(value, index)}
                        />
                        <Button onClick={() => toggleAccordionItem(index)}>
                            {item.isOpen ? 'Закрити' : 'Відкрити'}
                        </Button>
                        <Button onClick={() => deleteAccordionItem(index)}>Видалити</Button>
                    </div>
                ))}
            </div>
        );
    },
    save: function (props) {
        const { attributes } = props;

        return (
            <div className="accordion">
                {attributes.items.map((item, index) => (
                    <div>
                    <div className="accordion-line">
                        <span className="section-number">
                                {(index + 1).toString().padStart(2, '0')}
                            </span>
                    <div key={index} className={`accordion-section ${item.isOpen ? 'open' : ''}`}>

                        <a className="accordion-section-title" href={`#section${index + 1}`}>
                            <div className="accordion-title">{item.title}</div>
                            <div className="accordion-section-button">
                                <svg className='IconClose' style={{ display: item.isOpen ? 'block' : 'none' }} width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <rect width="48" height="48" rx="24" fill="black"/>
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M29.6569 19.7574L28.2426 18.3431L24 22.5858L19.7574 18.3431L18.3431 19.7574L22.5858 24L18.3431 28.2426L19.7574 29.6569L24 25.4142L28.2426 29.6569L29.6569 28.2426L25.4142 24L29.6569 19.7574Z" fill="white"/>
                                </svg>
                                <svg className='IconOpen' style={{ display: item.isOpen ? 'none' : 'block' }} width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <rect y="0.00100708" width="48" height="48" rx="24" fill="#F3F5F6"/>
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M25 17.001H23V23.001H17V25.001H23V31.001H25V25.001H31V23.001H25V17.001Z" fill="black"/>
                                </svg>

                            </div>
                        </a>

                        <div id={`section${index + 1}`}
                             className="accordion-section-content"
                             style={{ display: item.isOpen ? '' : 'none' }}>
                            <p>{item.content}</p>
                        </div>
                    </div>
                    </div>
                    </div>
                ))}
            </div>
        );
    },

});
